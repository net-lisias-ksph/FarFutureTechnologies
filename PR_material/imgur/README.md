# Far Future Technologies : UNOFFICIAL

## Slide 1

![](./zaTISMr.png)


## Slide 2

![](./4qlJ2Px.png)

An entry-level engine, this model is cheap to operate but soon outclassed by most other engines.


## Slide 3

![](./DfrXtpv.png)

Workhorse fusion engine, its key advantage is an integrated power reactor which make it a one-stop shop for propulsion needs.


## Slide 4

![](./XlF8xhc.png)

Longer variants of the Fresnel are better, but harder to get into orbit. Its integrated reactor isn't quite as good as the Discovery.


## Slide 4

![](./2C5EJTi.png)

Yeahhh it's a fusion aerospike. Compared to its fission-based cousin in Kerbal Atomics, it has a higher efficiency but lower thrust.


## Slide 5

![](./1oWihVs.png)

This engine is the king of Isp but lacks any kind of thrust.



## Slide 6

![](./YaZ4K1E.png)

BALANCE WARNING: This torch drive is the endgame fusion engine. That's all you need to know.


## Slide 7

![](./rEIbdif.png)

This is almost a torch drive. Very high thrust but lower efficiency.


## Slide 8

![](./1CphHse.png)

Novel engine that can't be refuelled. Ion engine-like thrust, but shockingly good specific impulse.



## Slide 9

![](./7juBG21.png)

Refuellable riff on the Clarke. Afterburner mode brings thrust from pitiful to just piddly.



## Slide 10

![](./wrp7c0R.png)

High thrust fission option. Pretty effects.


## Slide 11

![](./2xpUgiU.png)

Very similar to the Verne, trades a lower mass and higher efficiency for the need for small quantities of antimatter


## Slide 12

![](./WQJGBBE.png)


## Slide 13

![](./nWGxrJu.png)

BALANCE WARNING: Endgame antimatter engine. Truly ridiculous properties, but enormous.


## Slide 14

![](./jwkqp1j.png)

A set of switchable fission fuel tanks.


## Slide 15

![](./lRDxrw7.png)

Hey, more fission fuel tanks for radial uses.



## Slide 16

![](./wAx7E43.png)

Nuclear pulse units for fun and profit.


## Slide 17

![](./mfNdVR6.png)

Smaller fusion tanks.


## Slide 18

![](./UpjHE8N.png)

Larger fusion tanks.


## Slide 19

![](./Aoykleh.png)

Entry-level antimatter storage, large and unwieldy.



## Slide 20

![](./Pr11XfB.png)

Larger antimatter storage. Compact, more antimatter.


## Slide 21

![](./weDLCNz.png)

Largest antimatter storage.


## Slide 22

![](./k11zk3E.png)

Use the two fusion modes to switch between cheap, lower efficiency power generation and something better. Instant response makes this one easy to use.


## Slide 23

![](./eYLJIDg.png)

Use the two fusion modes to switch between cheap, lower efficiency power generation and something better. Instant response makes this one easy to use compared to a fission reactor.


## Slide 24

![](./O8TSPG4.png)

Detector (driven by the Space Dust mod) to detect atmospheric concentrations of He3 and Deuterium.


## Slide 25

![](./E96A0o4.png)

Detector for detecting naturally occurring trapped antimatter.


## Slide 26

![](./pKrvPgk.png)

If you like bleeding-edge engineering, developing a Jool cloud diver with this guy might be lots of fun here.


## Slide 27

![](./RiwvuZ3.png)

You can use this scoop to collect slow trickles of particles while in orbit.


## Slide 28

![](./S7CnN6r.png)

Beefy smelting equipment required for creating advanced nuclear fuels.


## Slide 29

![](./X4OUIci.png)

It's large and in charge. Use this 'drill' to extract Helium-3 from places like the Mun.


## Slide 30

![](./S4HzZRt.png)


## Slide 31

![](./S4HzZRt.png)


## Slide 32

![](./1cZew1o.png)

If you want a lot of antimatter, making it yourself might be the best option, though towing these out to Moho might be a bit tough...


## Slide 33

![](./Y5XBKzQ.mp4)

Engine effect showoff - Casaba igniting its fission pellets.


## Slide 34

![](./VVlexRP.mp4)

Frisbee doing its thing. Note: you would be very dead if you could see this.



## Slide 35

![](./1UcpHAE.mp4)

Hammertong fusing some things with lasers


## Slide 36

![](./YpmPBEo.mp4)

Heinlein being used in Kerbin's atmosphere, which is probably bad.


## Slide 37

![](![](./bBeUTKs.mp4)

Clarke sedately detonating nuclear pulses.


## Slide 38

![](./8HRtrVT.png)

Cascade departing Kerbin (Captain Sierra)


## Slide 39

![](./F2BDHV8.png)

Small Discovery-powered vessel (Clamp-0-Tron)


## Slide 40

![](./P7eiETl.png)

Cruising by some gas giant on fission fragments (coyotesfrontier)


## Slide 41

![](./EGruqjL.png)

Rapid crew shuttle using the Dirac (lemoncup)


## Slide 42

![](./NSHxMW4.png)

Fresnel-powered cruiser starting its journey to Laythe.


## Slide 43

![](./miprDLS.jpg)

Dual fusion reactors let you run almost any electric engine with ease.


## Slide 44

![](./aTseGHm.jpg)

Ok, so this is Ghengis Kerman, inspecting his space cruiser


## Slide 45

![](./bWW9I9O.jpg)

Kinda a Spaceballs kinda situation


## Slide 46

![](./A0XxuA6.jpg)

Several hours later, we reach the reaction chamber for inspection


## Slide 47

![](./9me50bf.jpg)

Looks good. Now all the way back.


## Slide 48

![](./uAPabzj.jpg)

Ignition!


## Slide 49

![](./Rzd4WqL.jpg)

Antimatter-shipping tug


## Slide 50

![](./RL2G02Q.jpg)

Antimatter generation station close to the local star.


## Slide 51

![](./zHhVSZR.jpg)

Deep space transport powered by a Hammertong


## Slide 52

![](./2jBK0hH.png)

Lifting off on fusion fire.


## Slide 53

![](./LvlqFpI.jpg)

SPEHSS BULLDOZER.
